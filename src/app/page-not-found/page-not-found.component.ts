import { Component } from '@angular/core';
import { ConfigService } from '../service/config.service';

@Component({
  selector: 'page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent {
  constructor(private _configService: ConfigService) {this._configService.sendTitle('404 PAGE NOT FOUND');}
}
