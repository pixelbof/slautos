import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../service/config.service';

@Component({
  selector: 'our-customers',
  templateUrl: './our-customers.component.html',
  styleUrls: ['./our-customers.component.css']
})
export class OurCustomersComponent {
  title = "Our Customers";
  constructor(private _configService: ConfigService) {this._configService.sendTitle(this.title.toUpperCase());}
}
