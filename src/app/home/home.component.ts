import { Component, OnInit } from '@angular/core';
import { ConfigService } from '../service/config.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  title = "Engineering, Plant and Paver Specialists";
  constructor(private _configService: ConfigService) {this._configService.sendTitle(this.title.toUpperCase());}
}
