import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ConfigService } from './service/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  title: any;
  subscription: Subscription;

  constructor(private _configService: ConfigService) {
      // subscribe to home component messages
      this.subscription = this._configService.getTitle().subscribe(_title => { this.title = _title.title;});
  }

  ngOnDestroy() {
      // unsubscribe to ensure no memory leaks
      this.subscription.unsubscribe();
  }
}
