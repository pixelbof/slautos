import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';


@Injectable()
export class ConfigService {
  private subject = new Subject<any>();

    sendTitle(_title: string) {
        this.subject.next({ title: _title });
    }

    clearTitle() {
        this.subject.next();
    }

    getTitle(): Observable<any> {
        return this.subject.asObservable();
    }
}
