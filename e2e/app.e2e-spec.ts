import { SlautosPage } from './app.po';

describe('slautos App', function() {
  let page: SlautosPage;

  beforeEach(() => {
    page = new SlautosPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
